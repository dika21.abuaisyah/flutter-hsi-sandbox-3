import 'dart:io';
import 'dart:math' as math;

void main(List<String> args) {
  Quiz1.kalkulator();
}

class Quiz1 {
  // instance
  double? num1;
  double? num2;
  
  // constructor
  Quiz1(double firstNum, double secondNum) {
    num1 = firstNum;
    num2 = secondNum;
  }

  double add(double firstNum, double secondNum) {
    return firstNum + secondNum;
  }

  double multiply(double firstNum, double secondNum) {
    return firstNum * secondNum;
  }

  static double log(double firstNum){
    return math.log(firstNum);
  }

  static double reamur(double firstNum){
    return firstNum * (4/5);
  }

  static double farenheit(double firstNum){
    return firstNum * (9/5) + 32;
  }

  static double kelvin(double firstNum){
    return firstNum + 273;
  }

  static void backToMainMenu() {
    print("\nKembali ke menu...\n");
    kalkulator();
  }

  static void kalkulator() {
    print("""Aplikasi Kalkulator
        \nNIP: ARN202-09007. 
        \nMenu: \n1. Penambahan\n2. Perkalian\n3. Log \n4. Konversi Suhu\n""");

    print("Pilihan?");
    String? operator = stdin.readLineSync();
  
    switch (operator) {
      case "1":
        print("\nSilahkan input angka ke-1");
        String? firstInput = stdin.readLineSync();    
        double firstNum = 0.0;
        firstNum = double.parse(firstInput!);
        
        print("\nSilahkan input angka ke-2");
        String? secondInput = stdin.readLineSync();
        double secondNum;
        secondNum = double.parse(secondInput!);
        Quiz1 calc1 = Quiz1(firstNum, secondNum);
        print("\n${calc1.num1} + ${calc1.num2} = ${calc1.add(firstNum, secondNum)}");
        backToMainMenu();
        break;
      case "2":
        print("\nSilahkan input angka ke-1");
        String? firstInput = stdin.readLineSync();    
        double firstNum = 0.0;
        firstNum = double.parse(firstInput!);
        
        print("\nSilahkan input angka ke-2");
        String? secondInput = stdin.readLineSync();
        double secondNum;
        secondNum = double.parse(secondInput!);
        Quiz1 calc1 = Quiz1(firstNum, secondNum);
        print("\n${calc1.num1} x ${calc1.num2} = ${calc1.multiply(firstNum, secondNum)}");
        backToMainMenu();
        break;
      case "3":
        print("\nSilahkan input angka");
        String? firstInput = stdin.readLineSync();    
        double firstNum;
        firstNum = double.parse(firstInput!);
        print("\nLog $firstNum = ${log(firstNum)}");
        backToMainMenu();
        break;
      case "4":
        print("\nSilahkan input derajat celcius");
        String? firstInput = stdin.readLineSync();    
        double firstNum;
        firstNum = double.parse(firstInput!);
        print("\nSuhu $firstNum celcius = \n${reamur(firstNum)} Reamur\n${farenheit(firstNum)} Farenheit\n${kelvin(firstNum)} Kelvin");
        backToMainMenu();
        backToMainMenu();
        break;
      default:
        print("Angka tidak ditemukan.");
        backToMainMenu();
        break;
    }
  }
}