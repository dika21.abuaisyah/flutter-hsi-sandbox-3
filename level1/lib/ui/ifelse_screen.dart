import 'package:flutter/material.dart';
import 'dart:core';

class IfElseScreen extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    var status;
    int nilai = 70;
    if(nilai<70){
      status = "Tidak Lulus";
    }else{
      status = "Lulus";
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text("Praktikum If Else"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Center(
          child: Column(
            children: [
              const Text("If Else", style: TextStyle(fontSize: 20),),
              const Text("Contoh: int nilai = 70;\nif(nilai<70){\nprint('Tidak Lulus');\n}else{\nprint('Lulus');\n}"),
              Text("Hasil: ${status}"),
            ]
            ),
          ),
        )
    );
  }
}