import 'package:flutter/material.dart';
import 'dart:core';

class WhileScreen extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Praktikum While"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Center(
          child: Column(
            children: [
              const Text("While", style: TextStyle(fontSize: 20),),
              const Text("Contoh: int i = 1;\nwhile(i<=10){\nprint(i++);\n}"),
              const Text("Hasil: "),
              getWidgets(),
            ]
            ),
          ),
        )
    );
  }
}

Widget getWidgets() {
    int i = 1;
    List<Widget> list = [];
    while (i <= 10) {
        list.add(Text(i.toString()));
        i++;
    }
    return Column(children:list);
}