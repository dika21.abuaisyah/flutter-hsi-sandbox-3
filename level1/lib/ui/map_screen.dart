import 'package:flutter/material.dart';
import 'dart:core';

List<String> letters = ['I', 'V', 'X'];
List<int> numbers = [1, 5, 10];

class MapScreen extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    Map<String, int> map = Map.fromIterables(letters, numbers);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Praktikum Map"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Center(
          child: Column(
            children:[
              const Text("Map", style: TextStyle(fontSize: 20),),
              const Text("Contoh: List<String> letters = ['I', 'V', 'X'];"),
              const Text("List<int> numbers = [1, 5, 10];"),
              const Text("Map<String, int> map = Map.fromIterables(letters, numbers);"),
              Text("Hasil: ${map}"),
            ]
          ),
        )
      ),
    );
  }
}