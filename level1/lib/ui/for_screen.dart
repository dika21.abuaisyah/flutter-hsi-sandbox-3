import 'package:flutter/material.dart';
import 'dart:core';

class ForScreen extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Praktikum For"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Center(
          child: Column(
            children:[
              const Text("For", style: TextStyle(fontSize: 20),),
              const Text("Contoh: for(var i=1;i<=10;i++){\nprint(i);\n}"),
              const Text("Hasil"),
              for(var i=1;i<=10;i++)
              Text("${i}"),
            ]
          ),
        )
      ),
    );
  }
}