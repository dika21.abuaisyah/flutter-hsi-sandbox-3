import 'package:flutter/material.dart';
import 'dart:core';

class SwitchCaseScreen extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    var command = 'OPEN';
    var status;
    switch (command) {
    case 'CLOSED':
      status = 'closed';
    case 'PENDING':
      status = 'pending';
      default:
       status = 'Andika';
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text("Praktikum Switch Case"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Center(
          child: Column(
            children: [
              const Text("Switch Case", style: TextStyle(fontSize: 20),),
              const Text("Contoh: var command = 'OPEN';\nswitch (command){\ncase 'CLOSED':\n print('closed');\n case 'PENDING':\n print('pending');\n    default:\n print('Andika');\n }"),
              Text("Hasil: ${status}"),
            ]
            ),
          ),
        )
    );
  }
}