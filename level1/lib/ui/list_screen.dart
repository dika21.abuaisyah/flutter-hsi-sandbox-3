import 'package:flutter/material.dart';
import 'dart:core';

var fixedLengthList1 = List<int>.filled(5, 0);
var fixedLengthList2 = List<int>.filled(5, 0);
var growableList1 = <String>['A', 'B'];
var growableList2 = <String>['A', 'B'];

class ListScreen extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    fixedLengthList2[0] = 87;
    growableList2[0] = 'G';
    return Scaffold(
      appBar: AppBar(
        title: const Text("Praktikum List"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          children:[
            const Text("Fixed-Length List", style: TextStyle(fontSize: 20),),
            Text("Contoh: Fixed-Length List ${fixedLengthList1}"),
            const Text("fixedLengthList[0] = 87;"),
            Text("Setelah ditambah angka 87 pada index pertama menjadi ${fixedLengthList2}"),
            const SizedBox(height: 20,),
            const Text("Growable List", style: TextStyle(fontSize: 20),),
            Text("Contoh: Growable List ${growableList1}"),
            const Text("growableList[0] = 'G';"),
            Text("Setelah diganti pada index pertama menjadi ${growableList2}"),
          ]
        )
      ),
    );
  }
}