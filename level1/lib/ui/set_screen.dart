import 'package:flutter/material.dart';
import 'dart:core';

Set<int> specialNumbers1 = Set();
Set<int> specialNumbers2 = Set();
Set<int> specialNumbers3 = Set();

class SetScreen extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    specialNumbers2.add(1);
    specialNumbers3.add(1);
    specialNumbers3.add(5);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Praktikum Sets"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Center(
          child: Column(
            children:[
              const Text("Sets", style: TextStyle(fontSize: 20),),
              Text("Contoh: Sets ${specialNumbers1}"),
              const Text("specialNumbers.add(1);"),
              Text("Hasil: ${specialNumbers2}"),
              const SizedBox(height: 20,),
              const Text("specialNumbers.add(5);"),
              Text("Setelah diganti pada index pertama menjadi ${specialNumbers3}"),
            ]
          ),
        )
      ),
    );
  }
}