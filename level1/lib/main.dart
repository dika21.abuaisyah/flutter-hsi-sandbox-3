import 'package:flutter/material.dart';
import 'ui/list_screen.dart';
import 'ui/map_screen.dart';
import 'ui/set_screen.dart';
import 'ui/for_screen.dart';
import 'ui/while_screen.dart';
import 'ui/ifelse_screen.dart';
import 'ui/switchcase_screen.dart';

void main() {
  runApp(MaterialApp(
    theme: ThemeData(
      appBarTheme: const AppBarTheme(
        backgroundColor: Colors.blue,
        foregroundColor: Colors.white //here you can give the text color
      ),
      textTheme: const TextTheme(
      displayLarge: TextStyle(
        fontSize: 72,
        fontWeight: FontWeight.bold,
      ),
    ),
    ),
    debugShowCheckedModeBanner: false,
    home: HomePage(),
  ));
}

class HomePage extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(
      backgroundColor: Colors.blueAccent,
      title: const Text("Tugas Level 1 Flutter HSI Sandbox 3.0",
        style: TextStyle(
          color: Colors.white
        ),
      ),
    ),
    body: GridView.count(
        crossAxisCount: 3,
        padding: const EdgeInsets.all(10),
        children: [
          InkWell(
            child: const Card(
              child: Center(child: Text("Praktikum List")),
            ),
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (_) => ListScreen()),
              );
            },
          ),
          InkWell(
            child: const Card(
              child: Center(child: Text("Praktikum Set")),
            ),
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (_) => SetScreen()),
              );
            },
          ),
          InkWell(
            child: const Card(
              child: Center(child: Text("Praktikum Map")),
            ),
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (_) => MapScreen()),
              );
            },
          ),
          InkWell(
            child: const Card(
              child: Center(child: Text("Praktikum Looping For")),
            ),
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (_) => ForScreen()),
              );
            },
          ),
          InkWell(
            child: const Card(
              child: Center(child: Text("Praktikum Looping While")),
            ),
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (_) => WhileScreen()),
              );
            },
          ),
          InkWell(
            child: const Card(
              child: Center(child: Text("Praktikum Percabangan dengan If Else")),
            ),
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (_) => IfElseScreen()),
              );
            },
          ),
          InkWell(
            child: const Card(
              child: Center(child: Text("Praktikum Percabangan dengan Switch Case")),
            ),
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (_) => SwitchCaseScreen()),
              );
            },
          ),
        ],
      )
  );
 }
}