import 'package:flutter/material.dart';
import 'package:quiz2/screens/home_screen.dart';
import 'package:quiz2/screens/akademi_screen.dart';
import 'package:quiz2/screens/reguler_screen.dart';
import 'package:quiz2/screens/profile_screen.dart';
import 'package:quiz2/widgets/bottom_navigation.dart';
import 'package:flutter_web_frame/flutter_web_frame.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key});

  @override
  Widget build(BuildContext context) {
    return FlutterWebFrame(
        builder: (context) {
          return MaterialApp(
          theme: ThemeData(
            useMaterial3: true,
          ),
          debugShowCheckedModeBanner: false,
          home: MyHomePage(),
        );
      },
      maximumSize: const Size(475.0, 812.0),
      backgroundColor: Colors.grey,
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;

  final List<Widget> _widgetOptions = <Widget>[
    const HomePage(),
    const AkademiPage(),
    const RegulerPage(),
    ProfilePage(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigation(
        currentIndex: _selectedIndex,
        onItemTapped: _onItemTapped,
      ),
    );
  }
}
