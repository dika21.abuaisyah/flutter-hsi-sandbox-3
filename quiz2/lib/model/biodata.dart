class Biodata {
  final String nowa;
  final String alamat;
  final String kota;
  final String status;

  const Biodata({
    required this.nowa,
    required this.alamat,
    required this.kota,
    required this.status,
  });
}