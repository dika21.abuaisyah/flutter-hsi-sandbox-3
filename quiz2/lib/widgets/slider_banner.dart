import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class ImageCarouselSlider extends StatelessWidget {
  final List<String> items;
  final CarouselController carouselController;
  final ValueChanged<int>? onIndexChanged;

  const ImageCarouselSlider({
    required this.items,
    required this.carouselController,
    this.onIndexChanged,
  });

  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      items: items.map((item) {
        return Container(
          margin: const EdgeInsets.all(5.0),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: Image.network(item, fit: BoxFit.cover, width: 1000.0),
          ),
        );
      }).toList(),
      options: CarouselOptions(
        autoPlay: true,
        aspectRatio: 2.0,
        onPageChanged: (index, reason) {
          if (onIndexChanged != null) {
            onIndexChanged!(index);
          }
        },
      ),
    );
  }
}