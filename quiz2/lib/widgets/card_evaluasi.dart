import 'package:flutter/material.dart';

class EvaluationCard extends StatelessWidget {
  const EvaluationCard({super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4),
        side: const BorderSide(
          color: Colors.black12,
        ),
      ),
      elevation: 0,
      color: Colors.transparent,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.blueAccent[100],
                  ),
                  padding: const EdgeInsets.all(4),
                  child: Text('Program Reguler', style: TextStyle(color: Colors.indigo[900], fontSize: 10,)),
                ),
                const Text('Selesai', style: TextStyle(color: Colors.blue),),
              ],
            ),
            const SizedBox(height: 10,),
            const Text('FI03.EH54', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15, fontFamily: 'Plus_Jakarta_Sans'),),
            const SizedBox(height: 10,),
            const Text('Silsilah Ilmiyyah Pembahasan Kitab Fadhlul Islam Bagian Ketiga', style: TextStyle(fontFamily: 'Plus_Jakarta_Sans'),),
            const SizedBox(height: 10,),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 0.2,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.blueAccent[100],
                  ),
                  padding: const EdgeInsets.all(2),
                  child: Text.rich(
                    TextSpan(
                      children: [
                        const WidgetSpan(
                          alignment: PlaceholderAlignment.middle,
                          child: Icon(Icons.list)
                        ),
                        TextSpan(
                          text: '2 Soal',
                            style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.indigo[900],
                            fontFamily: 'Plus_Jakarta_Sans',
                            fontSize: 10,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                const SizedBox(width:10),
                Container(
                  width: MediaQuery.of(context).size.width * 0.6,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.blueAccent[100],
                  ),
                  padding: const EdgeInsets.all(2),
                  child: Text.rich(
                    TextSpan(
                      children: [
                        const WidgetSpan(
                          alignment: PlaceholderAlignment.middle,
                          child: Icon(Icons.timer_off_outlined)
                        ),
                        TextSpan(
                          text: 'Jumat, 1 Mar 2024 . 20:00',
                            style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.indigo[900],
                            fontFamily: 'Plus_Jakarta_Sans',
                            fontSize: 10,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10,),
            InkWell(
              onTap: (){},
              child: Container(
                height: 50,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.black,
                  ),
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.white
                ),
                child: Center(
                  child: Text.rich(
                    TextSpan(
                      children: [
                        const WidgetSpan(
                          alignment: PlaceholderAlignment.middle,
                          child: Icon(Icons.play_arrow)
                        ),
                        TextSpan(
                          text: 'Dengarkan Audio',
                            style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.indigo[900],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: (){},
                  child: Container(
                    height: 50,
                    width: MediaQuery.of(context).size.width * 0.4,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.blue,
                      ),
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.blue
                    ),
                    child: const Center(
                      child: Text(
                          'Lembar Evaluasi',
                          style: TextStyle(
                            color: Colors.white
                          ),
                        ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: (){},
                  child: Container(
                    height: 50,
                    width: MediaQuery.of(context).size.width * 0.4,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black,
                      ),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: Center(
                      child: Text(
                          'Bukti Jawaban',
                          style: TextStyle(
                            color: Colors.indigo[900]
                          ),
                        ),
                    ),
                    ),
                ),
              ] 
            ),
          ],
        ),
      ),
    );
  }
}