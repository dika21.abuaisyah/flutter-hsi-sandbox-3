import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:quiz2/model/biodata.dart';

class ProfilePage extends StatelessWidget {
  final List<Biodata> biodatas = [
    const Biodata(
      nowa: '62-81368330786',
      alamat: "Jalan Rajawali II, No.1654, Kelurahan Talang Aman, Kecamatan Kemuning",
      kota: "KOTA PALEMBANG, SUMATERA SELATAN, 30128",
      status: "Menikah / 1",
    ),
  ];

  @override
Widget build(BuildContext context) {
  final biodata = biodatas[0];
  return Scaffold(
    appBar: AppBar(
      centerTitle: false,
      title: const Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'PROFIL',
            style: TextStyle(
              fontWeight: FontWeight.bold
            ),
          ),
          Text(
            'v.2402-2702',
            style: TextStyle(
              fontWeight: FontWeight.bold
            ),
          ),
        ],
      ),
      titleTextStyle: const TextStyle(
        fontFamily: 'Plus_Jakarta_Sans'
      ),
    ),
    body: CustomScrollView(
      slivers: [
        SliverList(
        delegate: SliverChildListDelegate(
          [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              Image.asset(
                                "logo/logo.png",
                                width: 40,
                                height: 40,
                              ),
                              const SizedBox(width: 5,),
                              const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Andika Widyanto',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text('ARN202-09007',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey
                                  ),
                                )
                              ],
                            ),
                            ],
                          )
                        ),
                        InkWell(
                          onTap: (){
                    
                          },
                          child: const Text.rich(
                            TextSpan(
                              children: [
                                WidgetSpan(
                                  child: Icon(FontAwesomeIcons.penToSquare)
                                  ),
                                TextSpan(
                                  text: 'Ubah',
                                    style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],),
                  ),
                  const Divider(color: Colors.grey),
                  ListTile(
                    leading: const Icon(FontAwesomeIcons.whatsapp),
                    title: const Text(
                      'Nomor Whatsapp',
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                    subtitle: Text(
                      biodata.nowa,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  ListTile(
                    leading: const Icon(Icons.home),
                    title: const Text(
                      'Alamat',
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                    subtitle: Text(
                      biodata.alamat,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  ListTile(
                    leading: const Icon(FontAwesomeIcons.mapPin),
                    title: const Text(
                      'Kabupaten/Kota, Provinsi, Kode Pos',
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                    subtitle: Text(
                      biodata.kota,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  ListTile(
                    leading: const Icon(FontAwesomeIcons.ring),
                    title: const Text(
                      'Status Pernikahan / Jumlah Anak',
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                    subtitle: Text(
                      biodata.status,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  const Divider(color: Colors.grey),
                  const ListTile(
                    leading: Icon(FontAwesomeIcons.database),
                    title: Text(
                      'Syahadah & Transkrip',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Row(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.8,
                          child: const Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Silsilah Ilmiyyah Pembahasan Kitab Fadhlul Islam Bagian Kedua',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text('REG-2024-S1',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              )
                            ],
                          ),
                        ),
                        InkWell(
                          onTap: (){
                    
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.blueAccent[100],
                            ),
                            padding: const EdgeInsets.all(4),
                            child: const Row(
                              children: [
                                Icon(Icons.download, color: Colors.blue,),
                                Text('Unduh', style: TextStyle(color: Colors.blue, fontSize: 10,))
                              ],
                            ),
                          ),
                        ),
                      ],),
                  ),
                  const Divider(color: Colors.grey),
                  const ListTile(
                    leading: Icon(FontAwesomeIcons.circleInfo),
                    title: Text(
                      'Info lainnya',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: InkWell(
                      onTap: (){},
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 50,
                        child: const Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Icon(Icons.lock_open_sharp),
                            Text('Ganti Password'),
                            SizedBox(width: 300),
                            Icon(Icons.arrow_forward_ios_outlined, size: 15,),
                          ],
                        )
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: InkWell(
                      onTap: (){},
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 50,
                        child: const Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Icon(Icons.info_outlined),
                            Text('Bantuan'),
                            SizedBox(width: 350),
                            Icon(Icons.arrow_forward_ios_outlined, size: 15,),
                          ],
                        )
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: InkWell(
                      onTap: (){},
                      child: Container(
                        height: 50,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.red,
                          ),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: const Center(
                          child: Text(
                              'Keluar',
                              style: TextStyle(
                                color: Colors.red
                              ),
                            ),
                        ),
                        ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      );
  }
}
