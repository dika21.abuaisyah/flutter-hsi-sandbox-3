import 'package:flutter/material.dart';

class RegulerPage extends StatelessWidget {
  const RegulerPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: Colors.indigo[900],
        title: const Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'REGULER',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
            Text(
              'v.2402-2702',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ],
        ),
        titleTextStyle: const TextStyle(
          fontFamily: 'Plus_Jakarta_Sans'
        ),
      ),
      body: const Center(
        child: Text('Akan Segera Hadir'),
      ),
    );
  }
}