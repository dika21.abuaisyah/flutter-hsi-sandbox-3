import 'package:flutter/material.dart';

class AkademiPage extends StatelessWidget {
  const AkademiPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: const Text(
            'AKADEMI',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          titleTextStyle: const TextStyle(
          fontFamily: 'Plus_Jakarta_Sans'
        ),
      ),
      body: const Center(
        child: Text('Segera Hadir'),
      ),
    );
  }
}