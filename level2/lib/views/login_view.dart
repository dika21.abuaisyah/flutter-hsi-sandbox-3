import 'package:flutter/material.dart';
import 'package:level2/views/home_view.dart';

class LoginView extends StatelessWidget {
  const LoginView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 25,
        ),
        child: ListView(
          children: [
            SizedBox(
              height: 150,
              child: Image.asset("assets/logo/logo-small.png",
                fit: BoxFit.contain,
              )
            ),
            TextField(
              decoration: InputDecoration(
                label: const Text("Email"),
                border: InputBorder.none,
                fillColor: Colors.grey[300],
                filled: true
              ),
            ),
            const SizedBox(height: 10,),
            TextField(
              decoration: InputDecoration(
                label: const Text("Password"),
                border: InputBorder.none,
                fillColor: Colors.grey[300],
                filled: true,
                suffixIcon: IconButton(
                  onPressed: (){},
                  icon: const Icon(
                    Icons.remove_red_eye_outlined,
                    color: Colors.grey,
                  ),
                )
              ),
            ),
            const SizedBox(height: 10,),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.6,
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (_) => const HomeView()));
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.red,
                  padding: const EdgeInsets.symmetric(
                    vertical: 20,
                  ),
                ),
                child: const Text("Login", 
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
            const SizedBox(height: 10,),
            TextButton(
              onPressed: (){}, 
              child: const Text("Forgot Password?"),
            ),
            const SizedBox(height: 10,),
            Row(
              children: [
                Expanded(
                  child: Container(
                    height: 1,
                    color: Colors.black,
                  )
                ),
                const SizedBox(width: 5,),
                const Text("or"),
                const SizedBox(width: 5,),
                Expanded(
                  child: Container(
                    height: 1,
                    color: Colors.black,
                  )
                ),
              ],
            ),
            const SizedBox(height: 10,),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.6,
              child: ElevatedButton(
                onPressed: () {},
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.blue,
                  padding: const EdgeInsets.symmetric(
                    vertical: 20,
                  ),
                ),
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.facebook, color: Colors.white,),
                    SizedBox(width: 5,),
                    Text("Login with Facebook", 
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 10,),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.6,
              child: ElevatedButton(
                onPressed: () {},
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.blue,
                  padding: const EdgeInsets.symmetric(
                    vertical: 20,
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("assets/logo/google.png"),
                    const SizedBox(width: 5,),
                    const Text("Login with Google", 
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 120,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text("Don't have an account?", 
                  style: TextStyle(color: Colors.black),
                ),
                TextButton(
                  onPressed: (){},
                  child: const Text("Sign Up"),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}