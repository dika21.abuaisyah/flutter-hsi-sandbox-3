import 'package:flutter/material.dart';
import 'package:level2/views/welcome_view.dart';

void main() => runApp(
  const MaterialApp(
    debugShowCheckedModeBanner: false,
    home: WelcomeView(),
  )
);